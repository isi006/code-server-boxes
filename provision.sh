#!/usr/bin/env bash

SYSTEM_NAME=$1
CODE_PASSWORD=$2

COMPOSE_VERSION=1.27.1

export DEBIAN_FRONTEND=noninteractive

# firewall
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow 2022
sudo ufw allow http
sudo ufw allow https
sudo ufw allow 8443
sudo ufw --force enable

# install additional packages
apt-get update -y

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    nginx \
    git \
    zip \
    unzip \
    software-properties-common


# install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update -y

apt-get install -y docker-ce docker-ce-cli containerd.io

curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


# add and authorize 'coder' user
adduser --disabled-password --gecos "" coder
usermod -aG sudo coder
usermod -aG docker coder

echo "ALL ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/99-all

mkdir -p /home/coder/.ssh
cp ~/.ssh/authorized_keys /home/coder/.ssh
chown coder:coder /home/coder/.ssh /home/coder/.ssh/authorized_keys

echo "export SYSTEM_NAME=${SYSTEM_NAME}" >> /home/coder/.bashrc


# install sdkman
sudo -i -u coder bash << EOF
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
EOF


# install code server
sudo -i -u coder bash << EOF
mkdir -p .config/code-server
curl -fsSL https://code-server.dev/install.sh | sh

cat <<EOT > .config/code-server/config.yaml
bind-addr: 127.0.0.1:6080
auth: password
password: ${CODE_PASSWORD}
cert: false
EOT

mkdir -p info
EOF

systemctl enable --now code-server@coder



# configure nginx as a reverse proxy

# forward to the code-server
cat <<EOT > /etc/nginx/conf.d/10_code.conf
server {
  listen 80;

  server_name code.${SYSTEM_NAME};

  return 301 https://\$host\$request_uri;
}

server {
  server_name code.${SYSTEM_NAME};
  
  listen 443 ssl http2;

  client_max_body_size 100M;

  proxy_buffering off;
  proxy_set_header X-Real-IP \$remote_addr;
  proxy_set_header X-forwarded-host \$host;
  proxy_set_header X-forwarded-Port 443;
  proxy_set_header X-forwarded-Proto https;
  proxy_set_header Host \$host;

  # needed for websockets
  proxy_set_header Upgrade \$http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_read_timeout 86400;

  ssl_session_timeout 5m;
  ssl_session_cache shared:SSL:5m;
  ssl_session_tickets off;

  ssl_certificate         /certs/cert.crt;
  ssl_certificate_key     /certs/private.key;
  ssl_trusted_certificate /certs/chain.pem;
  ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers             HIGH:!aNULL:!MD5;

  add_header Strict-Transport-Security "max-age=31536000" always;

  location / {
      proxy_pass http://127.0.0.1:6080/;
  }
}
EOT

# forward to the app under development on port 8080
cat <<EOT > /etc/nginx/conf.d/80_dev.conf
server {
  listen 80;

  server_name dev.${SYSTEM_NAME};

  return 301 https://\$host\$request_uri;
}

server {
  server_name dev.${SYSTEM_NAME};
  
  listen 443 ssl http2;

  client_max_body_size 100M;

  proxy_buffering off;
  proxy_set_header X-Real-IP \$remote_addr;
  proxy_set_header X-forwarded-host \$host;
  proxy_set_header X-forwarded-Port 443;
  proxy_set_header X-forwarded-Proto https;
  proxy_set_header Host \$host;

  # needed for websockets
  proxy_set_header Upgrade \$http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_read_timeout 86400;

  ssl_session_timeout 5m;
  ssl_session_cache shared:SSL:5m;
  ssl_session_tickets off;

  ssl_certificate         /certs/cert.crt;
  ssl_certificate_key     /certs/private.key;
  ssl_trusted_certificate /certs/chain.pem;
  ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers             HIGH:!aNULL:!MD5;

  add_header Strict-Transport-Security "max-age=31536000" always;

  location / {
      proxy_pass http://127.0.0.1:8080/;
  }
}
EOT


# forward to the apps proxy
cat <<EOT > /etc/nginx/conf.d/90_apps.conf
server {
  listen 80;

  server_name ~^(?<subdomain>[^.]+).${SYSTEM_NAME};

  return 301 https://\$host\$request_uri;
}

server {
  server_name ~^(?<subdomain>[^.]+).${SYSTEM_NAME};
  
  listen 443 ssl http2;

  client_max_body_size 100M;

  proxy_buffering off;
  proxy_set_header X-Real-IP \$remote_addr;
  proxy_set_header X-forwarded-host \$host;
  proxy_set_header X-forwarded-Port 443;
  proxy_set_header X-forwarded-Proto https;
  proxy_set_header Host \$host;

  # needed for websockets
  proxy_set_header Upgrade \$http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_read_timeout 86400;

  ssl_session_timeout 5m;
  ssl_session_cache shared:SSL:5m;
  ssl_session_tickets off;

  ssl_certificate         /certs/cert.crt;
  ssl_certificate_key     /certs/private.key;
  ssl_trusted_certificate /certs/chain.pem;
  ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers             HIGH:!aNULL:!MD5;

  add_header Strict-Transport-Security "max-age=31536000" always;

  location / {
      proxy_pass http://127.0.0.1:9080/;
  }
}
EOT

systemctl restart nginx


# change sshd port
echo "Port 2022" >> /etc/ssh/sshd_config
systemctl restart sshd
