# Einrichten eines dynamischen Web Proxies

Damit wir in unserer Umgebung einfach uns sicher Anwendungen nach außen anbieten können, sollten wir einen Proxy einrichten, der automatisch den Traffic zur richtigen Anwendung leitet.

Glücklicher müssen wir uns nicht um die Beantragung und Einrichtung von TLS Zertifikaten kümmern. Die Umgebung enthält bereits ein passendes Wildcard Zertifikat im Verzeichnis `/certs`. Dafür läuft bereits ein manuell konfigurierter Proxy, der die TLS-Terminierung übernimmt, den Traffic für unsere IDE korrekt weiterleitet (code-server läuft lokal auf http://localhost:6080) und den restlichen Traffic an http://localhost:9080 sendet. Hier wollen wir nun einen weiteren eigenen reverse Proxy in Betrieb nehmen, der sich automatisch konfiguriert, wenn Container gestartet werden, die Dienste nach außen anbieten wollen.

Hierfür gibt es ein sehr beliebtes container image: https://hub.docker.com/r/jwilder/nginx-proxy

## Netzwerk anlegen

Zunächst benötigen wir ein gemeinsames Netzwerk, in das wir alle Container hängen, die wir in diesem Kurs erzeugen. Startet in eurer Enwicklungsumgebung ein Terminal und erzeugt das Netzwerk mit folgenden Befehl:

    docker network create appnet

## Proxy Container starten

Dann brauchen wir einen Ort für unseren Infrastruktur Code. Z. B. ${HOME}/workspace/ci-environment. Dort legen wir dann einen entsprechenden Ordner for den Proxy an:

    mkdir -p ${HOME}/workspace/ci-environment/app-proxy

Dort legen wir eine einfache docker-compose.yml an, die einen Service mit dem o. g. Image enhält. Achtet darauf das ihr den Port 80 auf 9080 mapped und auch unser vorher angelegtes Netzwerk als externes Netzwerk verwendet.

Das könnte so aussehen:

```
version: '3'

services:
  nginx-proxy:
    image: jwilder/nginx-proxy
    restart: always
    ports:
      - "9080:80"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro

networks:
  default:
    external:
       name: appnet
```

Tipp: installiert euch die vscode Docker Erweiterung, damit erhaltet ihr in der IDE einen Überblick was in Docker geschieht.

Startet den Proxy mit:

    docker-compose up -d

## Funktion testen

Um nun zu testet, ob alles funktioniert können wir mal einen mini Workload starten. Legt hierfür wieder ein Verzeichnis an:

    mkdir -p ${HOME}/workspace/ci-environment/simple-hello

Und legt dort ein docker-compose.yml file an um einen Service mit dem Image nginxdemos/hello zu starten. Z. B. so:

```
version: '3'

services:
  hello:
    image: nginxdemos/hello
    environment:
      - VIRTUAL_HOST=hello.${SYSTEM_NAME}

networks:
  default:
    external:
       name: appnet
```

Hier ist es wieder wichtig das gemeinsame Netzwerk zu nutzen.

Außerdem müssen wir dem reverse Proxy noch mit mitteilen unter welchem Hostnamen (VIRTUAL_HOST) der Service von außen erreichbar sein soll. In diesem Fall ist es `hello.vm-X.devopslab.isium.de`. Die umgebung enthält bereits eine Variabel $SYSTEM_NAME die den öffentlichen Namen eures Systems enthält. 

Der Service sollte nun weltweit TLS gesichert ereichbar sein unter:

    https://hello.vm-X.devopslab.isium.de

Es sollte auch zu keine Zertifikatsfehlermeldung kommen.

## Troubleshooting

Zunächst sollte man prüfen ob der Proxy Containers korrekt gestartet ist.

    cd ${HOME}/workspace/ci-environment/app-proxy
    docker-compose ps

Außerdem könnte der Log des Proxy Containers aufschlussreich sein.

    docker-compose logs -f

Das Gleiche gilt natürlich für den Anwendungs Container.