# Jenkins konfigurieren

## Container starten

Nehmt bitte das jenkins image in Betrieb: https://hub.docker.com/r/jenkins/jenkins
Vorsicht: es gibt auf Docker Hub viele verschiedene Images, die aber Probleme mit sich bringen, das o. g. ist das Richtige.
Verwendet dafür docker-compose und ein named Volume, wie wir es zuvor auch schon taten.

Hier ein Vorschlag für docker-compose:
```
services:
  jenkins:
    image: jenkins/jenkins:lts
    restart: unless-stopped
    environment:
      - VIRTUAL_HOST=jenkins.${SYSTEM_NAME}
      - VIRTUAL_PORT=8080
    volumes:
      - jenkinsdata:/var/jenkins_home

volumes:
  jenkinsdata:

networks:
  default:
    external:
       name: appnet
```

Danach sollte man Jenkins unter https://jenkins.vm-X.devopslab.isium.de ereichen können. Das initial Passwort wird im Logfile ausgegben.


## Jenkins konfigrieren

Wir starten einfach erstmal mit den empfohlenen Plugins.

Nach der plugin Installation legen wir einen User an (z. B. admin).

## Funktion prüfen

Legt einen einfachen Job an der eine Shellausgabe macht und lasst ihn laufen.