# Gitea installieren

Als nächstes wollen wir uns die Git Umgebung gitea (https://gitea.io/) einrichten.

Dafür legen wir und wieder ein entprechendes Verzeichnis an:

    mkdir -p ${HOME}/workspace/ci-environment/gitea

Und erstellen ein docker-compose.yml file:

```
version: '3'

services:
  gitea:
    image: gitea/gitea
    environment:
      - VIRTUAL_HOST=gitea.${SYSTEM_NAME}
      - VIRTUAL_PORT=3000
    volumes:
      - webdata:/data
    ports:
      - "22:22"
    depends_on:
      - gitea-db
    restart: unless-stopped

  gitea-db:
    image: mariadb:10
    restart: unless-stopped
    environment:
      - MYSQL_ROOT_PASSWORD=<CHANGEME>
      - MYSQL_DATABASE=gitea
      - MYSQL_USER=gitea
      - MYSQL_PASSWORD=<CHANGEME>
    volumes:
      - dbdata/:/var/lib/mysql

volumes:
  webdata:
  dbdata:

networks:
  default:
    external:
       name: appnet
```

Tragt bitte sichere Passwörter für die Datenbank ein. Wichtig: Wir brauchen hier neben VIRTUAL_HOST auch VIRTUAL_PORT, da die Anwendung mehr als einen Port öffnet und der reverse Proxy wissen muss, welcher Port für http Traffic zuständig ist.

Wie euch vielleicht aufgefallen ist, werden hier Docker Volumes verwendet.

Nun solltet ihr die Anwendung unter https://gitea.$SYSTEM_NAME/install erreichen und einrichten können.

Bei der Einrichting müsst ihr einige abweichende Einstellungen vornehmen:

* Datenbanktyp ist MySQL
* Host ist gitea-db:3306 (so wie der Service im docker-compose.yml file heißt)
* Benutzername ist gitea
* Passwort ist das von euch eingestellte Passwort für MYSQL_PASSWORD
* Datenbankname bleibt bei gitea
* SSH-Server-Port bitte auf 22 lassen, den intern ist ja genau dieser Port geöffnet
* Gitea-Basis-URL auf die öffentliche Adresse des Systems einstellen, also https://gitea.vm-X.devopslab.isium.de

Danach landet ihr automatisch auf der Login Maske und könnt einen neuen User Registrieren, dieser wird dann automatisch zum Admin.

Wenn ihr als admin einloggt seit, solltet ihr als nächstes eine Organisation anlegen. Ihr solltet jetzt für jeden Benutzer eurer Gruppe einen User anlegen und ihn euerer Organosation zuordnen. 

Danach legen wir für diese Organisation ein Repository `ci-environment` an. Bitte 'Repository initialisieren' aktivieren, gitea funktioniert sonst nicht richtig (https://github.com/go-gitea/gitea/issues/2898).

Legt danach auf der Kommandozeile mit ssh-keygen einen ssh key ohne Passphrase an und fügt in euren persönlichen Einstellungen in gitea hinzu (Ausgabe von: `cat ~/.ssh/id_rsa.pub`).

Jetzt können wir das Repo clonen und die Datein die wir bereits angelegt haben hinzufügen aus dem Verzeichnis workspace/ci-environment hinzufügen.




