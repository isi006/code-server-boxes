# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "vm_count" {
  type = number
  default = 1
}

variable "domain_name" {
  type = string
}

variable "server_type" {
  type = string
  default = "cx11"
}

variable "code_password" {
  type = string
}
variable "managed_zone" {
  type = string
}

locals {
  system_name = "${terraform.workspace}.${var.domain_name}"
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

provider "google" {
  credentials = file("google-cloud.json")
  project     = "isi-labs"
  region      = "europe-west3"
}

resource "google_dns_record_set" "dns_record" {
  count = var.vm_count
  name = "vm-${count.index}.${local.system_name}."
  type = "A"
  ttl  = 10

  managed_zone = var.managed_zone

  rrdatas = [ hcloud_server.node[count.index].ipv4_address ]
}

resource "google_dns_record_set" "dns_record_wildcard" {
  count = var.vm_count
  name = "*.vm-${count.index}.${local.system_name}."
  type = "A"
  ttl  = 10

  managed_zone = var.managed_zone

  rrdatas = [ hcloud_server.node[count.index].ipv4_address ]
}

resource "hcloud_server" "node" {
  count = var.vm_count
  name = "vm-${count.index}.${local.system_name}"
  image = "ubuntu-20.04"
  location = "fsn1"
  ssh_keys = [ "${var.ssh_key}" ]
  server_type = var.server_type

  connection {
    type     = "ssh"
    user     = "root"
    host     = self.ipv4_address
  }

  provisioner "remote-exec" {
    when = create
    inline = [
      "mkdir -p /certs",
      "mkdir -p /root/code-server/config/code-server"
    ]
  }

  provisioner "file" {
    when = create
    source      = "provision.sh"
    destination = "/root/provision.sh"
  }

  provisioner "file" {
    when = create
    source      = "docker-compose.yml"
    destination = "/root/code-server/docker-compose.yml"
  }


  # create / renew certificates with dns challenge
  provisioner "local-exec" {
    when = create
    command = "./certbot.sh ${self.name}"
  }

  provisioner "local-exec" {
    when = create
    command = "rsync -e \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" -rltpv --delete config/${self.name}/certs/publish/ root@${self.ipv4_address}:/certs"
  }

  provisioner "remote-exec" {
    when = create
    inline = [
      "chmod +x /root/provision.sh",
      "/root/provision.sh ${self.name} ${var.code_password}${count.index}"
    ]
  }

}
