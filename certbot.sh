#!/usr/bin/env bash

DOMAIN=$1

docker run -i --rm  \
  -v "$(pwd)/config/${DOMAIN}/certs:/etc/letsencrypt" \
  -v "$(pwd)/config/${DOMAIN}/work:/var/lib/letsencrypt" \
  -v "$(pwd)/google-cloud.json:/google-cloud.json"  \
  -v "$(pwd)/certbot-run.sh:/certbot-run.sh"  \
  --entrypoint "/certbot-run.sh" \
  certbot/dns-google ${DOMAIN}


SRC=config/${DOMAIN}/certs/live/${DOMAIN}
DEST=config/${DOMAIN}/certs/publish

mkdir -p ${DEST}

cp -f ${SRC}/chain.pem ${DEST}/chain.pem
cp -f ${SRC}/fullchain.pem ${DEST}/cert.crt
cp -f ${SRC}/privkey.pem ${DEST}/private.key

