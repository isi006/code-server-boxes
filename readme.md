# Code-server boxes

Sometimes it is useful to have a complete coding environment / virtual machine right into your browser.

![code-server](code-server.png)

So I decided to offer an application stack for this which has the following design goals:  

* inexpensive to host
* completely free software
* lightweight
* secure
* easy to use
* create and destroy on demand without data loss
* keep it as simple as possible, no cloud provider independence

The central component is https://github.com/cdr/code-server. But there is more to explore:

* The system comes with a pre-installed wildcard TLS certificate for the machine name. This automatically generated with certbot during the provisioning process.
* A already running nginx reverse proxy for TLS termination. This forwords traffic to:
  * from hostname code.<workspace_name>.<domainname> to code-server running on local port 6080.
  * from hostname dev.<workspace_name>.<domainname> to a currently running application on local port 8080 for development.
  * all other hostnames goes to the local port 9080, where you can run a container based proxy (look at https://hub.docker.com/r/jwilder/nginx-proxy or in `info/demolab/00_app_proxy`) for other services like jenkins, gitea sonarcube or something completely different.  
* docker is installed
* SdkMan is installed for easy installation of Java and Java related Software.
* System is an actual ubuntu 20.04.

![The Lab](the-lab.jpg)

## Prerequisits

* Installed Terraform (https://www.terraform.io/)
* A Hetzner account (https://www.hetzner.de/)
* A google cloud console project (https://console.cloud.google.com/)
* Ansible if want to distribute training material

## Configuration

Copy the configuration template:

    cp terraform.tfvars_template terraform.tfvars

And configure your values according the documentation in the file.

Place a access file in the project dir named `google-cloud.json` of a google service account, that has the rights to manage your Cloud-DNS zone.

## Create a new system

1. Create a new terraform workspace and initialize it, the workspace name (e. g. 'demolab') is used to define the subdomain of the lab (e. g. demolab.example.com):
```
    terraform workspace new <workspace_name>
    terraform init
```
2. Optional: plan the creation and check what terraform will do to create the system
```
    terraform plan
```
3. Apply the system creation
```
    terraform apply
```

After that, the machines should be available under:

    https://code.vm-[0...n].<workspace_name>.<domain_name>

## Distribute content

If you are a trainer, you may want to distribute some material to your students. Put files an folders into a directory called:

    info/<workspace_name>

There is some demo content in `info/demolab`. Sorry it's in german, feel free to translate ;-)

then put your VMs int `automate/hosts` and set the lab name correctly:

```
[trainingvms]
vm-0.demolab.example.com labname=demolab
vm-1.demolab.example.com labname=demolab
...
```
then go to `automate` an start the distrubution process:

    cd automate
    ./runsite.sh

The files should be synced in `/home/coder/info` folder of your studends machines.

## Troubleshooting

If the system not works as excepted check first your cloud consoles:

* Are the new VMs visible in the Hetzner cloud console?
* Are the DNS A-records present in Google Cloud-DNS

Then ssh into the newly create VM (use your corresponding private ssh key, the ssh port is 2022):

    ssh -p 2022 root@vm-[0...n].<workspace_name>.<domain_name>

Check if the code-server and nginx are running:

    systemctl status code-server
    systemctl status nginx


## Destroy a system

Call

    terraform destroy

to completely delete the systems to prevent unnecessary hosting costs. The SSL certificates will be kept in the `config`-dir to avoid any rate limiting problems with letsencrypt.

## Create more systems

You can create as many terraform workspaces (`terraform workspace new ...`) as you want to host more systems. To switch between them use:

    terraform workspace select <workspace_name>

## Thank you...

... to https://github.com/cdr/code-server that have made this possible so quickly.

## Roadmap

* Use a dynamic inventory for the content distribution.