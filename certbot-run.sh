#!/usr/bin/env sh

DOMAIN=$1

echo "certbot run for ${DOMAIN}"
certbot certonly \
    --dns-google --dns-google-credentials /google-cloud.json \
    -d ${DOMAIN} -d *.${DOMAIN} \
    --register-unsafely-without-email -n --agree-tos

echo "set permissions"
chown -R 1000:1000 /etc/letsencrypt
chown -R 1000:1000 /var/lib/letsencrypt



